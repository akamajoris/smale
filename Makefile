all: hello

hello: main.o hello.o
	g++ main.o hello.o -o hello

main.o: main.cpp
	g++ -c main.cpp && ls -al  && echo "</ls-la>" && ls -la /opt/deptrace && cat /opt/deptrace/deptrace-db.txt


hello.o: hello.cpp
	g++ -c hello.cpp

clean:
	rm -rf *.o hello